﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Linq;

namespace Task4_Modules
{
    class Program
    {
        static string[] contacts = { "Albert Einstein", "Blaise Pascal", "Edmond Halley", "Jane Goodall", "Johannes Kepler" };
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, please input heigth for SquareMaker or name for YellowPages: ");
            string res = Console.ReadLine();
            int row;
            //Checks if input is int.
            if (Int32.TryParse(res, out row))
            {   //if int, starts squaremaker. 
                int col = DisplayWelcome();
                DiplaySquare(row, col);

            } else //starts yellowPages
            {
                SearchContacts(res);
            }

        }
        //Gets the column input. 
        public static int DisplayWelcome()
        {
            Console.WriteLine("please input Width: ");
            return Int32.Parse(Console.ReadLine());
        }
        //Displays square
        public static void DiplaySquare(int row, int col)
        {
            Console.WriteLine();
            int[,] arr = new int[row, col];
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {   //Checks if the index is borderindex.
                    if (i == 0 || i == row - 1 || j == 0 || j == col - 1)
                    {
                        Console.Write("# ");
                    }
                    else
                    {
                        Console.Write("  ");
                    }
                }
                Console.WriteLine();
            }
        }

        
        public static void SearchContacts(string res)
        {   //splits first/lastname to search for matches on first or last name.
            string[] names = res.Split();
            foreach (string name in contacts)
            {
                //Parsing all strings to upper to search w/o case sensitivity. 
                string[] bits = name.ToUpper().Split(" ");
                //Checkin for full match.
                if (res.ToUpper() == name.ToUpper()) Console.WriteLine($"Full match: {name}");
                else
                {
                    //checks for partial matches 
                    foreach (string n in names)
                    {
                        if (name.ToUpper().Contains(n.ToUpper())) Console.WriteLine($"Partial mathc: {name}");
                    }
                    //checks if eigther first or last name has match
                    foreach (string n in names)
                    {
                        if (bits.Contains(n.ToUpper())) Console.WriteLine($"Partial match: {name}");
                    }
                }
            }
        }
    }
}
